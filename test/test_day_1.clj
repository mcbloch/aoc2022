(ns test-day-1
  (:require
    [clojure.test :refer [deftest is testing]]
    [day-1 :refer [ex1 ex2]]))


(deftest complete
  (testing "Official input"
    (is (= 67622 (ex1 "res/input_1")))
    (is (= 201491 (ex2 "res/input_1")))))

(ns test-day-2
  (:require
    [clojure.test :refer [deftest is testing]]
    [day-2 :refer [ex1 ex2]]))


(deftest complete
  (testing "Official input"
    (is (= 11841 (ex1 "res/input_2")))
    (is (= 13022 (ex2 "res/input_2")))))

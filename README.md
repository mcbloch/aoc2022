# AoC 2022

My submissions for the [Advent of Code](https://adventofcode.com) 2022.

Written with [babashka](https://babashka.org/). A 'Fast native Clojure scripting runtime'.

See possible tasks like scaffolding, input fetching and easy code execution with
```
bb tasks
```

Run the challenge of a specific day with
```
bb day -day 1 -part 1
```
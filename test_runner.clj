#!/usr/bin/env bb

(ns test-runner
  (:require
    [babashka.classpath :as cp]
    [clojure.test :as t]))


(cp/add-classpath "src:test")


(require
  'test-day-1
  'test-day-2)


(defn test-results
  []
  (t/run-tests
    'test-day-1
    'test-day-2))


(defn failures-and-errors
  [test-results]
  (let [{:keys [fail error]} test-results]
    (+ fail error)))


(defn -main
  [& _args]
  (-> (test-results)
      failures-and-errors
      System/exit))


;; Same af the __name__ == "__main__" pattern in python
;; truthy when executed directly with ./test_runner.clj
(when (= *file* (System/getProperty "babashka.file"))
  (apply -main *command-line-args*))

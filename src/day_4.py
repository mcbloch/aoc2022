if "__main__" == __name__:
    count = 0
    count_b = 0

    for line_raw in open("res/input_4").readlines():
        line = line_raw.strip()
        [e1, e2] = line.split(",")
        [e11, e12] = e1.split("-")
        [e21, e22] = e2.split("-")

        e11 = int(e11)
        e12 = int(e12)
        e21 = int(e21)
        e22 = int(e22)

        if (e11 >= e21 and e12 <= e22) or (e21 >= e11 and e22 <= e12):
            count += 1

        if (
            (e11 >= e21 and e11 <= e22)
            or (e12 >= e21 and e12 <= e22)
            or (e21 >= e11 and e21 <= e12)
            or (e22 >= e11 and e22 <= e12)
        ):
            count_b += 1

    print(count)  # 605
    print(count_b)  # 914

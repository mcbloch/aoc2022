(ns day-2
  (:require
    [clojure.string :as str]))


(def A_i (int \A))
(def X_i (int \X))


(defn result-score
  [p1 p2]
  (+ 1 (* 3 (mod (+ 1 (* 2 p1) p2) 3)) p2))


(defn pick-score
  [p1 p2]
  (+ 1 (mod (+ 2 p1 p2) 3) (* 3 p2)))


(defn common
  [path score_fn]
  (->> path
       slurp
       str/split-lines
       (map #(let [a_i (- (int (.charAt %1 0)) A_i)
                   x_i (- (int (.charAt %1 2)) X_i)]
               (score_fn a_i x_i)))
       (apply +)))


(defn ex1
  [path]
  (common path result-score))


(defn ex2
  [path]
  (common path pick-score))

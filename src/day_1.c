#include <stdio.h>
#include <stdlib.h>

int ex1()
{
    FILE *fp = fopen("res/input_1", "r");

    char line[500];

    int sum = 0;
    int max = 0;

    while (fgets(line, sizeof(line), fp))
    {
        int i = atoi(line);
        if (i != 0)
        {
            sum = sum + i;
        }
        else
        {
            if (sum > max)
            {
                max = sum;
            }
            sum = 0;
        }
    }
    fclose(fp);

    return max;
}

int main()
{
    printf("Day 01 part 1: %d\n", ex1());
}
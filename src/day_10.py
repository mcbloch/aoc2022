from math import floor


def main():
    with open("res/input_10", "r") as f:
        data = f.read().splitlines()

        ans1 = 0

        cycle = 0
        reg_x = 1
        next_hold = 20
        for line in data:
            split = line.split(" ")
            cycle_step = None
            if split[0] == "noop":
                cycle_step = 1

            elif split[0] == "addx":
                cycle_step = 2

            for offset in range(cycle_step):
                col = floor((cycle + offset) % 40)
                if reg_x - 1 <= col <= reg_x + 1:
                    print("#", end="")
                else:
                    print(".", end="")
                if col == 39:
                    print()

            cycle += cycle_step

            if cycle >= next_hold:
                ans1 += next_hold * reg_x
                next_hold += 40

            if split[0] == "addx":
                reg_x += int(split[1])

        print(f"Day 10.01: {ans1}")  # 14320

        # PCPBKAPJ


if "__main__" == __name__:
    main()

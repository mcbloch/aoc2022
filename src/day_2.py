score_map_a = {
    # Rock
    "A": {
        "X": 3 + 1,
        "Y": 6 + 2,
        "Z": 0 + 3,
    },
    # Paper
    "B": {
        "X": 0 + 1,
        "Y": 3 + 2,
        "Z": 6 + 3,
    },
    # Scissors
    "C": {
        "X": 6 + 1,
        "Y": 0 + 2,
        "Z": 3 + 3,
    },
}

score_map_b = {
    "A": {
        # Lose
        "X": 0 + 3,
        # Draw
        "Y": 3 + 1,
        # Win
        "Z": 6 + 2,
    },
    # Paper
    "B": {
        "X": 0 + 1,
        "Y": 3 + 2,
        "Z": 6 + 3,
    },
    # Scissors
    "C": {
        "X": 0 + 2,
        "Y": 3 + 3,
        "Z": 6 + 1,
    },
}

score_a = 0
score_b = 0
for line in open("res/input_2").readlines():
    [p1, p2] = line.strip().split(" ")
    score_a += score_map_a[p1][p2]
    score_b += score_map_b[p1][p2]

print(score_a)
print(score_b)

-module(day_1).
-export([ex1/0]).

ex1() ->
    {ok, Data} = file:read_file("res/input_1"),
    io:format("Day 01 part 1: ~p~n", [
        lists:max(
            lists:map(
                fun(Str) ->
                    lists:sum(
                        lists:map(
                            fun(Line) -> list_to_integer(binary_to_list(Line)) end,
                            binary:split(string:chomp(Str), [<<"\n">>], [global])
                        )
                    )
                end,
                binary:split(Data, [<<"\n\n">>], [global])
            )
        )
    ]).

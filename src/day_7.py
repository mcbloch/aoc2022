from __future__ import annotations

from dataclasses import dataclass, field
from pprint import pp
from typing import Optional


@dataclass
class Directory:
    name: str
    parent: Optional[Directory] = None
    children: dict[str, Directory | File] = field(default_factory=lambda: dict())
    size: int = 0

    def __repr__(self):
        children_str = list(
            map(
                lambda c: (str(c.size) if isinstance(c, File) else "dir")
                + " "
                + c.name,
                self.children,
            )
        )
        return f"D {self.name} ({self.size}): {children_str}"


@dataclass
class File:
    parent: Directory
    size: int
    name: str

    def __repr__(self):
        return f"F {self.name} ({self.size})"


def print_dir(dir, indent):
    for c in dir.children:
        pre = " " * indent
        type_str = "file" if isinstance(c, File) else "dir"
        print(pre + f"- {c.name} ({type_str}, size={c.size})")
        if isinstance(c, Directory):
            print_dir(c, indent + 2)


def gather_directories(directory, directories):
    for c in directory.children.values():
        if isinstance(c, Directory):
            directories.append(c)
            gather_directories(c, directories)


def main():
    with open("res/input_7", "r") as f:
        # with open("res/input_7_test", "r") as f:
        current_dir = Directory(name="/")
        current_path = [current_dir]

        content = f.read()
        commands = content.split("$ ")[1:]
        commands = list(map(lambda c: c.strip().split("\n"), commands))

        for execution in commands:
            command = execution[0]
            output = execution[1:]
            if command[0:2] == "cd":
                if command[3] == "/":
                    current_dir = current_path[0]
                    current_path = [current_dir]
                elif command[3:5] == "..":
                    current_path.pop()
                    current_dir = current_path[len(current_path) - 1]
                else:
                    new_dir_name = command[3:]
                    new_dir = current_dir.children[new_dir_name]
                    current_path.append(new_dir)
                    current_dir = new_dir
            if command[0:2] == "ls":
                previous_size = current_dir.size
                current_dir.size = 0
                # NOTE I do not handle changed content.
                # It is assumed ls always returns the same content
                for child in output:
                    parts = child.split(" ")
                    if parts[0] == "dir":
                        child_dir_name = parts[1]
                        if child_dir_name not in current_dir.children:
                            child_dir = Directory(name=child_dir_name)
                            child_dir.parent = current_dir
                            current_dir.children[child_dir_name] = child_dir
                        else:
                            child_dir = current_dir.children[child_dir_name]
                        current_dir.size += child_dir.size
                    else:
                        size, name = parts
                        child_file = File(parent=current_dir, name=name, size=int(size))
                        current_dir.children[name] = child_file
                        current_dir.size += child_file.size

                traversal_dir = current_dir
                while traversal_dir.name != "/":
                    parent = traversal_dir.parent
                    previous_parent_size = parent.size
                    parent.size -= previous_size
                    parent.size += traversal_dir.size

                    traversal_dir = parent
                    previous_size = previous_parent_size

        # print_dir(Directory(name='', children={'/': current_path[0]}), 0)
        root = current_path[0]
        directories = [root]
        gather_directories(root, directories)
        sum = 0
        for directory in directories:
            if directory.size <= 100000:
                sum += directory.size

        print(f"Day 07.01: {sum}")

        total_disk_space = 70000000
        needed_unused_disk_space = 30000000
        space_left = total_disk_space - root.size

        smallest_deletable = None
        for directory in directories:
            if directory.size + space_left > needed_unused_disk_space:
                if (
                    smallest_deletable is None
                    or directory.size < smallest_deletable.size
                ):
                    smallest_deletable = directory

        print(f"Day 07.02: {smallest_deletable.size}")


if "__main__" == __name__:
    main()

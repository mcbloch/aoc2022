#!/usr/bin/env lua

local function ex1()
    local sum = 0
    local max = 0
    for line in io.lines("res/input_1") do
        if line == nil or line == '' then
            if sum > max then
                max = sum
            end
            sum = 0
        else
            sum = sum + tonumber(line)
        end
    end

    return max
end

print(string.format("Day 01 part 1: %d", ex1()))

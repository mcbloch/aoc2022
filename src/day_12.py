from typing import List, Tuple


def find_char(data: List[List[str]], char: str):
    # Find the start marker
    for x in range(len(data[0])):
        for y in range(len(data)):
            if data[y][x] == char:
                return x, y


def find_chars(data, char):
    chars = []
    for x in range(len(data[0])):
        for y in range(len(data)):
            if data[y][x] == char:
                chars.append((x, y))
    return chars


def heuristic(from_pos, to_pos):
    # return (to_pos[1] - from_pos[1]) ** 2 + (to_pos[0] - from_pos[0]) ** 2
    return 0


def get_grid_height(grid: List[List[str]], pos: Tuple[int, int]) -> str:
    return grid[pos[1]][pos[0]]


def astar(data, start, end, maximum_length=None):
    open_list = [start]
    closed_list = []
    g_cost = {start: 0}
    h_cost = {start: heuristic(start, end)}
    f_cost = {start: 0}
    parent = {start: None}

    while len(open_list) > 0:
        # Look for the lowest f-cost
        k = open_list[0]
        lowest_f = f_cost[k]
        for item in open_list[1:]:
            if f_cost[item] < lowest_f:
                k, lowest_f = item, f_cost[item]

        current_height = get_grid_height(data, k)

        open_list.remove(k)
        closed_list.append(k)

        if k == end:
            # Recreate the path
            path = []
            while k is not None:
                path.append(k)
                k = parent[k]
            path.reverse()
            return path

        for direction in [[1, 0], [0, 1], [-1, 0], [0, -1]]:
            new_pos = (k[0] + direction[0], k[1] + direction[1])
            # Out of bounds check
            if (
                new_pos[0] < 0
                or new_pos[1] < 0
                or new_pos[0] >= len(data[0])
                or new_pos[1] >= len(data)
            ):
                continue

            # To high to travel to check
            new_height = get_grid_height(data, new_pos)
            if ord(new_height) > ord(current_height) + 1:
                continue

            # In closed list check
            if new_pos in closed_list:
                continue

            if maximum_length and g_cost[k] + 1 > maximum_length:
                continue

            if new_pos not in open_list:
                open_list.append(new_pos)
                parent[new_pos] = k
                g_cost[new_pos] = g_cost[k] + 1
                h_cost[new_pos] = heuristic(new_pos, end)
                f_cost[new_pos] = g_cost[new_pos] + h_cost[new_pos]
            else:
                if g_cost[parent[new_pos]] + 1 > g_cost[k] + 1:
                    parent[new_pos] = k
                    g_cost[new_pos] = g_cost[k] + 1
                    f_cost[new_pos] = g_cost[new_pos] + h_cost[new_pos]
    return None


def main():
    with open("res/input_12", "r") as f:
        data = f.read().strip().splitlines()
        for i, l in enumerate(data):
            # print(l)
            data[i] = list(l.strip())

        start = find_char(data, "S")
        data[start[1]][start[0]] = "a"
        end = find_char(data, "E")
        data[end[1]][end[0]] = "z"

        path = astar(data, start, end)
        # for y, line in enumerate(data):
        #     print(
        #         "".join([c if (x, y) not in path else c.upper() for x, c in enumerate(line)])
        #     )
        # print(path)
        len_part_1 = len(path)

        print(f"Day 12.01: {len_part_1 - 1}")  # 456

        shortest_len = len_part_1
        for start in find_chars(data, "a"):
            path = astar(data, start, end, shortest_len)
            if path is None:
                continue
            if len(path) < shortest_len:
                shortest_len = len(path)

        print(f"Day 12.02: {shortest_len - 1}")  # 454 to high


if "__main__" == __name__:
    main()

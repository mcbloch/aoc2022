if "__main__" == __name__:
    with open("res/input_6", "rb") as f:
        data = f.read()

        found_packet = False
        found_message = False

        i = 3
        start_i = 0

        while not (found_message and found_packet):
            if not found_packet:
                match = data.find(data[i], start_i, i)
                if match >= 0:
                    start_i = match + 1
                elif i - start_i == 3:
                    print(f"Day 06.01 {i+1}")
                    found_packet = True

            if found_packet and not found_message:
                match = data.find(data[i], start_i, i)
                if match >= 0:
                    start_i = match + 1
                elif i - start_i == 13:
                    print(f"Day 06.02 {i+1}")
                    found_message = True

            i += 1
# 1804
# 2508

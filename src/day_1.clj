(ns day-1
  (:require
    [clojure.string :refer [split-lines split blank?]]))


(defn ex1
  [data_path]
  (as-> data_path v
        (slurp v)
        (split v #"\r?\n\r?\n")
        (map #(->> %
                   split-lines
                   (map read-string)
                   (apply +)) v)
        (apply max v)))


(defn ex2
  [data_path]
  (as-> data_path v
        (slurp v)
        (split v #"\r?\n\r?\n")
        (map #(->> %
                   split-lines
                   (map read-string)
                   (apply +)) v)
        (sort v)
        (take-last 3 v)
        (apply + v)))


(defn ex1-first-try
  [data_path]
  (let [data (->> data_path
                  slurp
                  split-lines)]
    (:best (reduce (fn [{sum :sum best :best} val]
                     (if (blank? val)
                       (if (> sum best)
                         {:sum 0 :best sum}
                         {:sum 0 :best best})
                       (let [num (read-string val)]
                         {:sum (+ sum num) :best best})))
                   {:sum 0 :best 0}
                   data))))


(defn ex2-first-try
  [data_path]
  (let [data (->> data_path
                  slurp
                  split-lines)]
    (reduce + (:best (reduce (fn [{sum :sum best :best} val]
                               (if (blank? val)
                                 (let [split (split-with (partial > sum) (sort best))]
                                   (if (empty? (first split))
                                     {:sum 0 :best best}
                                     {:sum 0 :best (concat (rest (first split)) [sum] (second split))}))
                                 (let [num (read-string val)]
                                   {:sum (+ sum num) :best best})))
                             {:sum 0 :best [0 0 0]}
                             data)))))

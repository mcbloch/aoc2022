import functools
import os
from typing import List, Optional, Tuple


def try_compare(l1, l2) -> Optional[bool]:
    # print(f"Compare {l1} vs {l2}")
    if isinstance(l1, int) and isinstance(l2, int):
        if l1 < l2:
            return True
        if l1 > l2:
            return False
        return None

    elif isinstance(l1, list) and isinstance(l2, list):
        for i in range(max(len(l1), len(l2))):
            if i >= len(l1):
                return True
            if i >= len(l2):
                return False

            val = try_compare(l1[i], l2[i])
            if val is not None:
                return val

            if i + 1 == len(l1) and i + 1 == len(l2):
                return None

    elif isinstance(l1, list) and isinstance(l2, int):
        return try_compare(l1, [l2])
    elif isinstance(l1, int) and isinstance(l2, list):
        return try_compare([l1], l2)
    else:
        raise Exception("Unexpected state")
    
    return None


def try_compare_cmp(l1, l2) -> int:
    if try_compare(l1, l2):
        return 1
    else:
        return -1


def main():
    with open("res/input_13", "r") as f:
        # IO
        # --
        data = f.read().strip().split(os.linesep * 2)
        packets = []
        sum_of_right_orders_pairs = 0
        for pair in data:
            [l1_val, l2_val] = pair.split(os.linesep)
            packets.append(eval(l1_val))
            packets.append(eval(l2_val))

        # Part 1
        # ------
        for i in range(0, len(packets) // 2):
            if try_compare(packets[i * 2], packets[i * 2 + 1]):
                sum_of_right_orders_pairs += i + 1
                # print("Right order")
            # else:
            # print("Wrong order")
            # print()

        print(f"Day 13.01: {sum_of_right_orders_pairs}")  # 6187

        # Part 2
        # ------
        packets.append([[2]])
        packets.append([[6]])

        packets.sort(key=functools.cmp_to_key(try_compare_cmp), reverse=True)

        ans2 = (packets.index([[2]]) + 1) * (packets.index([[6]]) + 1)

        print(f"Day 13.02: {ans2}")  # 23520


if "__main__" == __name__:
    main()

def score_c(c):
    value = 0
    if c > "Z":
        value = ord(c) - ord("a") + 1
    else:
        value = ord(c) - ord("A") + 27
    return value


if "__main__" == __name__:
    sum_1 = 0
    sum_2 = 0
    group = []
    for line_raw in open("res/input_3").readlines():
        line = line_raw.strip()
        s1 = set(line[: int(len(line) / 2)])
        s2 = set(line[int(len(line) / 2) :])

        common = list(s1.intersection(s2))[0]
        sum_1 += score_c(common)

        if len(group) < 3:
            group.append(set(line))

        if len(group) == 3:
            print(group)
            i = group[0].intersection(group[1])
            i2 = i.intersection(group[2])
            print(i2)
            print(score_c(list(i2)[0]))
            sum_2 += score_c(list(i2)[0])
            group.clear()

    print(sum_1)  # 8240
    print(sum_2)  # 2587

import copy
import os

if "__main__" == __name__:

    with open("res/input_5") as f:
        data = f.read()

        [stack_data, move_data] = data.split(2 * os.linesep)
        stack_data = stack_data.split("\n")
        stack_data.reverse()
        stacks = [[] for i in range(9)]
        for line in stack_data[1:]:
            for i in range(9):
                box = line[i * 4 + 1]
                if box != " ":
                    stacks[i].append(box)

        stacks_a = stacks
        stacks_b = copy.deepcopy(stacks)

        for line in move_data.strip().split(os.linesep):
            line = line.strip()
            [_, amount, _, origin, _, destination] = line.split(" ")
            amount, origin, destination = int(amount), int(origin), int(destination)
            temp = []
            for _ in range(amount):
                item_a = stacks_a[origin - 1].pop()
                stacks_a[destination - 1].append(item_a)

                item_b = stacks_b[origin - 1].pop()
                temp.append(item_b)

            temp.reverse()
            for item in temp:
                stacks_b[destination - 1].append(item)

        print("".join(stack[len(stack) - 1] for stack in stacks_a))
        print("".join(stack[len(stack) - 1] for stack in stacks_b))

import math
import os
from dataclasses import dataclass
from math import floor
from typing import List, Tuple


@dataclass
class Monkey:
    items: List[int]
    operation: str
    test_divisible_by: int
    throw: Tuple[int, int]

    def process(self, monkeys: List["Monkey"], part: int, lcm: int):
        inspection_count = len(self.items)
        for item in self.items:
            new = self.operation(item)
            if part == 0:
                new = floor(new / 3)

            new %= lcm
            if new % self.test_divisible_by == 0:
                monkeys[self.throw[0]].items.append(new)
            else:
                monkeys[self.throw[1]].items.append(new)
        self.items.clear()
        return inspection_count


def main():
    with open("res/input_11", "r") as f:
        data = f.read().split(os.linesep * 2)

        monkeys = []
        monkey_start_items = []
        for i, monkey_str in enumerate(data):
            monkey_str = monkey_str.split(os.linesep)
            monkey_start_items.append(
                list(map(int, monkey_str[1].split(":")[1].strip().split(",")))
            )
            monkey = Monkey(
                items=monkey_start_items[i],
                operation=eval("lambda old: " + monkey_str[2].split("=")[1].strip()),
                test_divisible_by=int(monkey_str[3].split(" ")[-1]),
                throw=(
                    int(monkey_str[4][-1]),
                    int(monkey_str[5][-1]),
                ),
            )
            monkeys.append(monkey)

        lcm = math.lcm(*list(map(lambda m: m.test_divisible_by, monkeys)))

        for part_i in range(2):
            rounds = [20, 10000][part_i]
            inspection_counts = [0 for _ in range(len(monkeys))]
            for _ in range(rounds):
                for i, monkey in enumerate(monkeys):
                    inspection_count = monkey.process(monkeys, part_i, lcm)
                    inspection_counts[i] += inspection_count

            inspection_counts.sort(reverse=True)
            ans = inspection_counts[0] * inspection_counts[1]
            print(f"Day 11.0{part_i + 1}: {ans}")  # 108240 # 25712998901

            for i, monkey in enumerate(monkeys):
                monkey.items = monkey_start_items[i]


if "__main__" == __name__:
    main()

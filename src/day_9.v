import os
import math { clamp }
import vsl.la { vector_add }
import arrays

fn main() {
	direction_vector_map := {
		'U': [0, 1]
		'D': [0, -1]
		'L': [-1, 0]
		'R': [1, 0]
	}
	knot_count := 10

	mut data := os.read_lines('res/input_9')!
	mut knots := [][]int{len: knot_count, init: []int{len: 2, init: 0}}
	mut visited := [][][]int{len: 2, init: [][]int{len: 500, init: []int{len: 500, init: 0}}}
	visited[0][0][0] = 1
	visited[1][0][0] = 1
	for line in data {
		dirvec := direction_vector_map[line[0..1]]
		for _ in 0 .. line[2..line.len].int() {
			knots[0] = vector_add(1, knots[0], 1, dirvec)

			for i in 1 .. knot_count {
				diff := vector_add(1, knots[i - 1], -1, knots[i])

				if diff[0] > -2 && diff[0] < 2 && diff[1] > -2 && diff[1] < 2 {
					continue
				}

				knots[i][0] += int(clamp(diff[0], -1, 1))
				knots[i][1] += int(clamp(diff[1], -1, 1))
				if i == 1 || i == 9 {
					visited[i % 9][(knots[i][0] + 500) % 500][(knots[i][1] + 500) % 500] = 1
				}
			}
		}
	}
	mut count_1 := 0
	mut count_9 := 0
	for x in 0 .. visited[0].len {
		count_1 += arrays.sum[int](visited[1][x])!
		count_9 += arrays.sum[int](visited[0][x])!
	}
	println(count_1)
	println(count_9)
}

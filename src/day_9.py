import numpy as np
from numpy import array

direction_vector_map = {
    "U": array([0, 1], dtype=int),
    "D": array([0, -1], dtype=int),
    "L": array([-1, 0], dtype=int),
    "R": array([1, 0], dtype=int),
}


def print_grid(knots):
    max_y = max((knot[1] for knot in knots))
    max_x = max((knot[0] for knot in knots))
    for y in range(max(max_y + 2, 4) - 1, -1, -1):
        for x in range(max(max_x + 2, 4)):
            c = "."
            idxs = np.where(np.all(knots == np.array([x, y]), axis=1))
            if len(idxs[0]) > 0:
                c = str(idxs[0][0])
            print(c + " ", end="")
        print()
    print()


def main():
    with open("res/input_9", "r") as f:
        data = f.read().splitlines()
        knot_count = 10
        knots = np.zeros((knot_count, 2), dtype=int)
        # 400x400 seems to be big enough for the given input
        visited = np.zeros((2, 400, 400), dtype=bool)
        visited[0, 0, 0] = True
        visited[1, 0, 0] = True
        # print_grid(knots)

        for line in data:
            dirvec = direction_vector_map[line[0]]
            for _ in range(int(line[2:])):

                knots[0] += dirvec
                for i in range(1, knot_count):
                    diff = knots[i - 1] - knots[i]
                    if -2 < diff[0] < 2 and -2 < diff[1] < 2:
                        continue

                    knots[i] += np.core.umath.clip(diff, -1, 1)

                    if i == 1 or i == 9:
                        visited[i % 9, knots[i][0], knots[i][1]] = True

                # print_grid(knots)
        print(f"Day 09.01: {np.count_nonzero(visited[1])}")  # 6376
        print(f"Day 09.02: {np.count_nonzero(visited[0])}")  # 2607


if "__main__" == __name__:
    main()

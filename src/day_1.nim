import strutils
import std/strformat

proc ex1(): int =
    let f = open("res/input_1")
    # Close the file object when you are done with it
    defer: f.close()
    var line = ""

    var sum = 0
    var max = 0

    while f.readLine(line):
        if line == "":
            if sum > max:
                max = sum
            sum = 0
        else:
            sum += parseInt(line)
    return max
    

echo fmt"Day 01 part 1: {ex1()}"
(ns utils)


(defn debug
  [x]
  (prn x)
  x)


(defmacro debugM
  [expr]
  `(let [x# ~expr] ; Save the result of the expression so it isn't evaluated twice
     (println '~expr "\n\t" x#)
     x#))

use std::fs::File;
use std::io::{self, BufRead};

fn ex1() -> u64 {
    let file = File::open("res/input_1").unwrap();
    let lines = io::BufReader::new(file).lines();

    let mut sum: u64 = 0;
    let mut max: u64 = 0;

    // Consumes the iterator, returns an (Optional) String
    for line in lines {
        if let Ok(str) = line {
            if let Ok(num) = str.parse::<u64>() {
                sum += num;
            } else {
                if sum > max {
                    max = sum;
                }
                sum = 0;
            }
        }
    }
    return max;
}

fn main() {
    println!("Day 01 part 1: {}", ex1());
}

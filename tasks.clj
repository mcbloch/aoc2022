#!/usr/bin/env bb

(ns tasks
  (:require
    [babashka.classpath :as cp]
    [babashka.cli :as cli]
    [babashka.curl :as curl]
    [clojure.java.io :as io]))


(cp/add-classpath "src")


;; (defn help-fn [task spec]
;;   (println (str "bb " task))
;;   (println (cli/format-opts {:spec spec})))

(defn error-fn
  [{:keys [spec type cause msg option] :as data}]
  (if (= :org.babashka/cli type)
    (case cause
      :require
      (println
        (format "Missing required argument:\n%s"
                (cli/format-opts {:spec (select-keys spec [option])})))
      (println msg))
    (throw (ex-info msg data)))
  (System/exit 1))


(def spec
  {:day {:desc "Day of the challenge you want to target"
         :ref "<val>"
         :coerce :int}
   :part {:desc "Part of the day. 1 or 2"
          :ref "<val>"
          :coerce :int}
   :year {:desc "Year of the adventofcode. Default is 2022"
          :ref "<val>"
          :coerce :int
          :default 2022}})


(defn get_input
  {:org.babashka/cli {:require [:day :year]
                      :spec spec
                      :error-fn error-fn}}
  [{day :day year :year}]
  ;; (when (or (contains? m :help) (contains? m :h)) (help-fn "input" spec) (System/exit 1))
  ;; (println m)
  (let [cookie (System/getenv "AoCCookie")
        filename (str "res/input_" day)]
    (println (format "Downloading input for day %s, edition %s to file ./%s" day year filename))
    (let [res (curl/get (format "https://adventofcode.com/%s/day/%s/input" year day)
                        {:headers {"cookie" cookie}
                         :throw false})]
      (if (= 404 (:status res))
        (do
          (println "Could not fetch input file.")
          (println "=====")
          (println (:body res)))
        (->>
          res
          :body
          (spit filename))))))


(defn day_template
  [day]
  (format "(ns day-%s)

(defn ex1
  [data_path]
  (let [data (slurp data_path)]
    (println \"Nop\")))


(defn ex2
  [data_path]
  (let [data (slurp data_path)]
    (println \"Nop\")))
" day))


(defn test_template
  [day]
  (format "(ns test-day-%s
  (:require
    [clojure.test :refer [deftest is testing]]
    [day-%s :refer [ex1 ex2]]))


(deftest complete
  (testing \"Official input\"
    (is (= \"todo\" (ex1 \"res/input_%s\")))
    (is (= \"todo\" (ex2 \"res/input_%s\")))))
" day day day day))


(defn new_day
  [{day :day}]
  (doseq [[path content]
          [[(format "src/day_%s.clj" day) (day_template day)]
           [(format "test/test_day_%s.clj" day) (test_template day)]]]
    (if (.exists (io/file path))
      (println (format "File at path %s already exists, not creating a new one." path))
      (do
        (spit path content)
        (println (format "Created new source file at %s" path))))))


(defn run_day
  {:org.babashka/cli {:require [:day :part]
                      :spec spec
                      :error-fn error-fn}}
  [{day :day part :part}]
  ;; (assert (and (> (Integer/parseInt day) 0) (< (Integer/parseInt day) 26)) "Day should be between 1 and 25")
  ;; (assert (or (= (Integer/parseInt part) 1) (= (Integer/parseInt part) 2)) "Part should be 1 or 2")
  (require 'day_1)
  (require 'day_2)
  (let [path (format "res/input_%s" day)]
    (assert (.exists (io/file path)) (format "Code file not found for day %s" day))
    (println ((resolve (symbol (format "day-%s/ex%s" day part))) path))))


(defmacro sectime
  [expr]
  `(let [start# (. System (currentTimeMillis))
         runs 100
         ret# (dotimes [_ runs] ~expr)
         elapsed (/ (double (- (. System (currentTimeMillis)) start#)) 1000.0)]
     (println (str "Average elapsed time for " runs " runs: " (/ elapsed runs) " secs"))
     (println (str "Test ran for a total duration of " elapsed " secs"))
     ret#))


(defn time_day
  {:org.babashka/cli {:require [:day :part :year]
                      :spec spec
                      :error-fn error-fn}}
  [{day :day part :part}]
  ;; (assert (and (> (Integer/parseInt day) 0) (< (Integer/parseInt day) 26)) "Day should be between 1 and 25")
  ;; (assert (or (= (Integer/parseInt part) 1) (= (Integer/parseInt part) 2)) "Part should be 1 or 2")
  (require 'day_1)
  (require 'day_2)
  (let [path (format "res/input_%s" day)
        day_fn (resolve (symbol (format "day-%s/ex%s" day part)))]
    (assert (.exists (io/file path)) (format "Code file not found for day %s" day))

    (sectime
      (day_fn path))))

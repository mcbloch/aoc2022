#!/bin/sh

echo "  C   "
echo "======"

gcc src/day_1.c -o day_1
hyperfine './day_1'
rm day_1


echo
echo "  RUST  "
echo "========"
rustc src/day_1.rs
hyperfine './day_1'
rm day_1

echo
echo "  NIM  "
echo "======="
nim c src/day_1.nim
hyperfine './src/day_1'
rm src/day_1

echo
echo "  LUA  "
echo "======="
hyperfine 'lua src/day_1.lua'

echo
echo "  Erlang  "
echo "=========="
erlc src/day_1.erl
hyperfine 'erl -noshell -noinput -s day_1 ex1 -s init stop'
rm day_1.beam
